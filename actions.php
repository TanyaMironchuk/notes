<?php
session_start();
?>
<html>
<head>
    <title>Заметки</title>
    <meta charset="utf-8">
</head>
<body>
<?php
class Note
{
    public $name;
    public $text;
    public $date;
}
//добавление записи
if (isset($_POST['add']))
{
    if (!empty($_POST['text']))
    {$note = new Note();
        $note->name = strip_tags($_POST['name']);
        $note->text = strip_tags($_POST['text']);
        $note->date = date("d.m.Y");
        $_SESSION['arr'][] = $note;
    echo "Заметка добавлена.<br>";}
    else{echo "Не введена заметка<br>";}
    echo  "<a href='index.php'>Вернуться на главную</a>";

}
//просмотр всех записей
if (isset($_POST['all']))
{
    if (!empty($_SESSION['arr']))
    {
        echo "<h2>Все ваши записи</h2>";
        echo "<table border='1px' cellpadding='0' cellspacing='0' width='500px'>
				<tr>
						<td><b>Название</b></td>
						<td><b>Текст</b></td>
						<td><b>Дата</b></td>
						<td></td>
				</tr>";

        $arr = $_SESSION['arr'];
        foreach ($arr as $key=>$a)
        {
            echo "<tr><td> <pre>$a->name</pre> </td><td><pre> $a->text </pre></td><td> $a->date </td>";
            echo "<td><form action='' method='post'><input type='submit' name='del$key' value ='Удалить' > </form></td></tr>";

        }
        echo "</table>";
    }
    else
    {echo "Записей нет.<br>";}
    echo "<br><a href='index.php'>Вернуться на главную</a>";
}

//удаление всех записей
if (isset($_POST['del_all']) )
{
    session_destroy();
    echo "Все заметки удалены. <a href='index.php'>Вернуться на главную</a>";
}

//удаление одной записи
if (!empty($_SESSION))
{
    $arr = $_SESSION['arr'];
    foreach ($arr as $key => $a)
    {
        if (isset($_POST['del'.$key]))
        {
            $name = $a->name;
            unset($_SESSION['arr'][$key]);
            echo "Заметка с именем '$name' удалена успешно.<br> <a href='index.php'>Вернуться на главную</a>";
            break;
        }
    }
}
?>


</body>
</html>